using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera : MonoBehaviour
{
 

    [SerializeField] private Transform target;
    float rotate;
 
    void Start()
    {
        rotate = 0f;

 
    }


    void Update()
    {

    }

    public void UpdateCamera()
    {
        transform.position = new Vector3(transform.position.x, transform.position.y + 1.5f, transform.position.z);
        rotate = rotate + 1f;

        if (rotate == 10f)
        {
            transform.eulerAngles = Vector3.forward * 90;
        }

        if(rotate == 20f)
        {
            transform.eulerAngles = Vector3.forward * -0;
        }

        if (rotate == 30f)
        {
            transform.eulerAngles = Vector3.forward * 180;
        }

        if (rotate == 40f)
        {
            transform.eulerAngles = Vector3.forward * -90;
        }

        if (rotate == 60f)
        {
            transform.eulerAngles = Vector3.forward * +180;
        }

        if (rotate == 65f)
        {
            transform.eulerAngles = Vector3.forward * +90;
        }

        if (rotate == 70f)
        {
            transform.eulerAngles = Vector3.forward * 0;
        }

        if (rotate == 85f)
        {
            transform.eulerAngles = Vector3.forward * +90;
        }

        if (rotate == 95f)
        {
            transform.eulerAngles = Vector3.forward * -90;
        }



    }
}
