using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Point : MonoBehaviour
{
    public int Scorev;
    public Text score;
    



    // Start is called before the first frame update
    void Start()
    {
       
        Scorev = 0;
        score.text = "Score: " + Scorev;

    }
    
    // Update is called once per frame
    void Update()
    {
      
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals("Player"))

        {

            Scorev = Scorev + 1;
            score.text = "Score: " + Scorev;
            Debug.Log(Scorev);

        }

        if (Scorev == 100f)
        {          
            score.text = "You Win";
            SceneManager.LoadScene("SampleScene");
        }

    }

}
