using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Square : MonoBehaviour
{

    public Rigidbody2D rgby;
    public float moveSpeed;
    public Vector2 force;
  




    void Start()
    {

        rgby = GetComponent<Rigidbody2D>();
        
    }

    // Update is called once per frame
     void Update()
    {
      
 
            if(gameObject.tag.Equals("Finish"))
        {
            GetComponent<Rigidbody2D>().gravityScale = 2f;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {

        if (collision.gameObject.tag.Equals("Finish"))
        {

            gameObject.tag = "Finish";
            GetComponent<Rigidbody2D>().gravityScale = 2f;
      

        }

    }

        void OnTriggerEnter2D(Collider2D collision)
        {


            if (collision.gameObject.tag.Equals("Freeze"))
            {
            
                rgby.constraints = RigidbodyConstraints2D.FreezePosition;
                rgby.constraints = RigidbodyConstraints2D.FreezeRotation;
        }


        }


    
}




